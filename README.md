# Livres de Proches — CoverSpider

This little tool helps you to automate cover generation for every missing cover
in your shelves on [Livres de Proches].

## Configuration

Move `.env.example` into `.env` and set your username and password to connect to
[Livres de Proches].

### Environment variables

- `LDP_ROOT_URL`: root URL (default: https://www.livresdeproches.fr)
- `LDP_TIMEOUT`: timeout in seconds for DOM element crawling (default: 20)
- `LDP_SHELVES_COUNT`: number of shelves you have on LdP (default: 1)
- `LDP_USERNAME`: your username on LdP
- `LDP_PASSWORD`: your password on LdP
- `FORCE_COVER`: enforce existing cover regeneration if activated (default: 0)

## Installation

You need Ruby to be installed. And Bundler. If not present, just install it with:

~~~sh
gem install bundler
~~~

Then install the project dependencies:

~~~sh
bundle install
~~~

You also need [ChromeDriver] to be installed and accessible from your `$PATH`.
Just download it, extract it, and move it into a convinient directory:

~~~sh
mv ~/Downloads/chromedriver /usr/local/bin/chromedriver
~~~

## Usage

~~~sh
./bin/covers
~~~

~~~log
bundle exec ruby config.ru
+ bundle exec ruby config.ru
I, [2019-06-04 10:05:14 +0200#15450] [M: 70355356164140]  INFO -- generate_cover_spider: Spider: started: generate_cover_spider
D, [2019-06-04 10:05:15 +0200#15450] [M: 70355356164140] DEBUG -- generate_cover_spider: BrowserBuilder (selenium_chrome): created browser instance
D, [2019-06-04 10:05:15 +0200#15450] [M: 70355356164140] DEBUG -- generate_cover_spider: BrowserBuilder (selenium_chrome): enabled window_size
D, [2019-06-04 10:05:15 +0200#15450] [M: 70355356164140] DEBUG -- generate_cover_spider: BrowserBuilder (selenium_chrome): enabled native headless_mode
I, [2019-06-04 10:05:15 +0200#15450] [M: 70355356164140]  INFO -- generate_cover_spider: Browser: started get request to: https://www.livresdeproches.fr
2019-06-04 10:05:15 WARN Selenium [DEPRECATION] :driver_path is deprecated. Use :service with an instance of Selenium::WebDriver::Service instead.
I, [2019-06-04 10:05:17 +0200#15450] [M: 70355356164140]  INFO -- generate_cover_spider: Browser: finished get request to: https://www.livresdeproches.fr
I, [2019-06-04 10:05:17 +0200#15450] [M: 70355356164140]  INFO -- generate_cover_spider: Info: visits: requests: 1, responses: 1
D, [2019-06-04 10:05:18 +0200#15450] [M: 70355356164140] DEBUG -- generate_cover_spider: Browser: driver.current_memory: 438236
I, [2019-06-04 10:05:18 +0200#15450] [M: 70355356164140]  INFO -- generate_cover_spider: > Logged in.
I, [2019-06-04 10:05:27 +0200#15450] [M: 70355356164140]  INFO -- generate_cover_spider: > Skip cover for Couverture du livre Tao Te Ching
I, [2019-06-04 10:05:27 +0200#15450] [M: 70355356164140]  INFO -- generate_cover_spider: > Skip cover for Couverture du livre Monnayé (Discworld, #36; Moist Von Lipwig, #2)
I, [2019-06-04 10:05:27 +0200#15450] [M: 70355356164140]  INFO -- generate_cover_spider: > Skip cover for Couverture du livre 1984
…
I, [2019-06-04 10:06:49 +0200#15450] [M: 70355356164140]  INFO -- generate_cover_spider: > Cover generated is https://www.livresdeproches.fr/media/img/no-cover.svg
I, [2019-06-04 10:15:03 +0200#15928] [M: 70260820549700]  INFO -- generate_cover_spider: > Cover generated is https://products-images.di-static.com/image/terry-pratchett-les-annales-du-disque-monde-au-guet/9782266099707-200x303-1.jpg
I, [2019-06-04 10:15:14 +0200#15928] [M: 70260820549700]  INFO -- generate_cover_spider: > Cover generated is https://www.livredepoche.com/sites/default/files/styles/manual_crop_269_435/public/images/livres/couv/9782253144458-001-T.jpeg?itok=OYCTl_48
I, [2019-06-04 10:15:37 +0200#15928] [M: 70260820549700]  INFO -- generate_cover_spider: > Cover generated is https://products-images.di-static.com/image/isaac-asimov-l-homme-bicentenaire/9782207250259-200x303-1.jpg
I, [2019-06-04 10:15:44 +0200#15928] [M: 70260820549700]  INFO -- generate_cover_spider: > Done.
I, [2019-06-04 10:15:44 +0200#15928] [M: 70260820549700]  INFO -- generate_cover_spider: Browser: driver selenium_chrome has been destroyed
I, [2019-06-04 10:15:44 +0200#15928] [M: 70260820549700]  INFO -- generate_cover_spider: Spider: stopped: {:spider_name=>"generate_cover_spider", :status=>:completed, :error=>nil, :environment=>"development", :start_time=>2019-06-04 10:11:03 +0200, :stop_time=>2019-06-04 10:15:44 +0200, :running_time=>"4m, 41s", :visits=>{:requests=>1, :responses=>1}, :items=>{:sent=>0, :processed=>0}, :events=>{:requests_errors=>{}, :drop_items_errors=>{}, :custom=>{}}}
~~~

Alternatively, you can run it in a real browser:

~~~sh
HEADLESS=false ./bin/covers
~~~

![image](https://framagit.org/akarzim/livresdeproches/uploads/ad5fbbaf3f6c2427b5a75838d659220f/image.png)

[Livres de Proches]: https://www.livresdeproches.fr
[ChromeDriver]: https://sites.google.com/a/chromium.org/chromedriver/
