require "dotenv/load"
require "kimurai"

class GenerateCoverSpider < Kimurai::Base
  ROOT_URL = ENV.fetch("LDP_ROOT_URL", "https://www.livresdeproches.fr")
  TIMEOUT = ENV.fetch("LDP_TIMEOUT", 20).to_f
  SHELVES_COUNT = ENV.fetch("LDP_SHELVES_COUNT", 1).to_s
  USERNAME = ENV.fetch("LDP_USERNAME")
  PASSWORD = ENV.fetch("LDP_PASSWORD")
  FORCE_COVER = ENV.fetch("FORCE_COVER", 0)

  @name = "generate_cover_spider"
  @engine = :selenium_chrome
  @config = { window_size: [1366, 768] }
  @start_urls = [ROOT_URL]

  def parse(response, url:, data: {})
    wait = Selenium::WebDriver::Wait.new(timeout: TIMEOUT)

    browser.within(:css, ".main-menu-accueil") do
      browser.click_on("Se connecter")
    end

    browser.within(:id, "account-form-log") do
      browser.fill_in("email", with: USERNAME)
      browser.fill_in("password", with: PASSWORD)
      browser.click_on("Je me connecte")
      logger.info "> Logged in."
    end

    browser.find("//span[@class='compteur-etagere']", text: SHELVES_COUNT, wait: TIMEOUT)

    # Update response to current response after interaction with a browser
    response = browser.current_response

    response.xpath("//span[@class='book_visual ']").each do |span_book|
      x_span_book = Nokogiri::CSS.xpath_for(span_book.css_path).first

      browser.within "#{x_span_book}//image-book-widget" do
        cover = browser.find(:css, "img.book-cover-img")
        browser.scroll_to(cover)

        if skip_cover? && browser.has_no_xpath?("img[@src='#{ROOT_URL}/media/img/no-cover.svg']", wait: 1)
          logger.info "> Skip cover for #{cover[:alt]}"
        else
          browser.find(x_span_book).click

          browser.within "//details-book-widget" do
            browser.click_on("Modifier le livre/mon avis")
          end

          browser.within "//edit-book-widget" do
            browser.click_on("Générer la couverture")

            begin
              browser.find(:id, "loading_img", visible: true, wait: TIMEOUT)
              wait.until { !browser.find(:id, "loading_img").visible? }
            rescue Selenium::WebDriver::Error::TimeoutError
              logger.info "> Cover not found."
            rescue Capybara::ElementNotFound => e
              logger.warn e.message
            else
              cover = browser.find(:css, "img.preview")[:src]
              logger.info "> Cover generated is #{cover}"
            ensure
              browser.click_on("Enregistrer")
            end
          end
        end
      end
    end

    logger.info "> Done."
  end

  private

  def skip_cover?
    FORCE_COVER == 0
  end
end
